const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const morgan = require('morgan');
const router = require('./routes/index.routes');
const { authenticate } = require('./middlewares/authenticate');
const { failureResponse } = require('./helpers/responses');
const db = require('./db');
const PORT = process.env.PORT || 4032;

const app = express();
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(morgan('dev'));

(async () => {
  try {
    await db.sequelize.authenticate();
    console.log("Connected to Postgres DB...");
  }
  catch (e) {
    console.error(e);
  }
})();

app.use('/api/v1', authenticate, router);

app.use(function(_, res) {
  failureResponse(res, 404, {"message": "Endpoint not found"});
});

app.listen(PORT, () => console.log(`Listening on PORT: ${PORT}`));
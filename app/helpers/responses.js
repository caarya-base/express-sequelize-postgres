const successResponse = (res, status, data) => {
  return res.status(status).send({ ...data, success: true });
};

const failureResponse = (res, status, data) => {
  return res.status(status).send({ ...data, success: false });
};

module.exports = { successResponse, failureResponse };

const authenticate = (req, res, next) => {
  // Write authentication middleware
  // header config
  // Authorization: "Bearer "+token
  // X-AUTH-TOKEN: token
  next();
};

module.exports = { authenticate };
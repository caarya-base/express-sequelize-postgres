const { successResponse, failureResponse } = require('../helpers/responses');

const getUsers = async (req, res) => {
  return successResponse(res, 200, {"users": []});
};

module.exports = { getUsers };